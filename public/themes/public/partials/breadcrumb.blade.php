<?php 

	$activeCrumb = Cache::remember('breadcrumb-'.Request::path(), 60, function () {
		$repoMenu = app('Litepie\Contracts\Menu\MenuRepository');
		$rootMenus = $repoMenu->rootMenues();
		$activeMenu = null;
		foreach ($rootMenus as $menu)
		{
			$submenu = $repoMenu->scopeQuery(function ($query) {
				return $query->orderBy('order', 'ASC');
			})->all()->toMenu($menu->key);
			foreach ($submenu as $menuchild)
			{
				if ($menuchild->active)
				{
					$activeMenu = $menuchild;
					break;
				}
			}
			if ($activeMenu)
				break;
		}
		
		return $activeMenu;
	});

	Theme::breadcrumb()->setTemplate('
	<ul class="breadcrumb">
    <?php foreach ($crumbs as $i => $crumb):?>
        @if ($i != (count($crumbs) - 1))
        <li><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a><span class="divider"></span></li>
        @else
        <li class="active">{{ $crumb["label"] }}</li>
        @endif
    <?php endforeach; ?>
	</ul>');

?>

@if($activeCrumb)
	@include('public::breadcrumb.head', array('menu' => $activeCrumb))
@endif

@if (count(Theme::breadcrumb()->crumbs)>0)
	<div id="area-breadcrumb">
		{!! Theme::breadcrumb()->render() !!}
	</div>
@endif