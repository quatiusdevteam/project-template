<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Litepie\Support\Facades\Theme;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Request;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        TokenMismatchException::class
    ];

     /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $e
     *
     * @return void
     */
    public function report(Exception $e)
    {
        
        return parent::report($e);
    }
    
    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        
        if($request->wantsJson()){
            switch($e) {
                case $e instanceof ModelNotFoundException:
                    return response()->json([
                        'exception' =>get_class($e),
                        'message' => 'Record not found',
                    ], 404);
                    break;
                case $e instanceof NotFoundHttpException:
                    return response()->json([
                        'exception' =>get_class($e),
                        'message' => 'Page not found',
                    ], 404);
                    break;
                case $e instanceof HttpException:
                    return response()->json([
                        'exception' =>get_class($e),
                        'message' => $e->getMessage(),
                    ], $e->getStatusCode());
                    break;
                case $e instanceof TokenMismatchException: 
                    return response()->json([
                        'exception' =>get_class($e),
                        'message' => 'The token has expired, please try again.',
                        'pageRedirect' => url()->previous()
                    ], 301);
                default:{
                    return response()->json([
                        'exception' =>get_class($e),
                        'message' => $e->getMessage(),
                    ], 500);
                }
            } 
        }else{
            switch($e) {
                case $e instanceof NotFoundHttpException:
                    return config('app.debug')?parent::render($request, $e):redirect('/');
                case $e instanceof TokenMismatchException: 
                    return back()->with('error','The token has expired, please try again.');
                default:
                    return parent::render($request, $e);
            }
            
        }
        return parent::render($request, $e);
    }
    
    /**
     * Render the given HttpException.
     *
     * @param  \Symfony\Component\HttpKernel\Exception\HttpException  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpExceptionInterface $e)
    {
        $status = $e->getStatusCode();
        
        $theme = Request::is('admin/*')?'admin':'public';

        if (view()->exists($theme."::errors.404")) {
            return Theme::uses(config('theme.map.'.$theme.'.theme'))->layout(config('theme.map.'.$theme.'.blank'))->of($theme."::errors.{$status}", ['exception' => $e])->render();
        }

        return parent::renderHttpException($e);
        
    }
}
