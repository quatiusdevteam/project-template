<?php namespace App\Modules\Home\Controllers;

use App\Http\Controllers\PublicWebController;
use Illuminate\Http\Request;
use Quatius\Admin\Controllers\Content\PagePublicWebController;

class HomeController extends PublicWebController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$page = app('Lavalite\Page\Interfaces\PageRepositoryInterface')->getPage("home.guest");
			
		if (is_null($page)) {
			return $this->theme->of('Home::index')->render();
		}
		return PagePublicWebController::renderPage($this->theme, $page);
	}
}
