<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    {!! Meta::tag('robots') !!}

    {!! Meta::tag('site_name', config('app.name', 'Web Development')) !!}
    {!! Meta::tag('url', Request::url()); !!}
    {!! Meta::tag('locale', 'en_EN') !!}

    {!! Meta::tag('title', config('app.name', 'Web Development')) !!}
    {!! Meta::tag('description') !!}
	<meta name="keywords" content="{{Theme::place('keywords')}}">
    
    {!! Meta::tag('image', Theme::asset()->url('img/logo.png')) !!}
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{Theme::asset()->url('favicon.ico')}}">

    <title>{{ Meta::get('title', config('app.name', 'Web Development')) }}</title>

 	{!! Theme::asset()->styles() !!}
	<script src="{{asset('packages/jquery/js/jquery.min.js')}}"></script>
	
 	{!! Theme::asset()->container('last')->styles() !!}
  </head>

  <body>
	{!! Theme::partial('header') !!}
	<div class="container system-alert">
		{{--include('flash::message')--}}
	</div>
	@if(Theme::has('showcase'))
		<div class="showcase col-xs-12">
	    	{!!Theme::place('showcase')!!}
		</div>
	@endif
<div class="container">
	<div id="area-content">
	{!! Theme::partial('breadcrumb') !!}
	
	@if(Theme::has('content-header'))
	<div class="content-header">
		{!!Theme::place('content-header')!!}
	</div>
	
	@endif
	{!! Theme::content() !!}
	
	@if(Theme::has('content-footer'))
	<div class="content-footer">
		{!!Theme::place('content-footer')!!}
	</div>
	@endif
	
    </div>    
</div>	
<div class="col-xs-12">
	@if(Theme::has('page-bottom'))
	<div class="page-bottom" style="display: inline-block ; width:100%">
		{!!Theme::place('page-bottom')!!}
	</div>
	@endif
</div>
<div id="footer" class="col-xs-12">
    {!! Theme::partial('footer') !!}
</div>
</div>
    <!-- Bootstrap core JavaScript
    
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster <script type="text/javascript" src="{{asset(elixir('js/vendor_public.js')) }}"></script>
     -->
    <script type="text/javascript">
    var baseUrl = "{{url('/')}}";
    </script>
    {!! Theme::asset()->scripts() !!}
	
    {!! Theme::asset()->container('footer')->scripts() !!}

  </body>
</html>
