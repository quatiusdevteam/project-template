<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
            [
                'id'   => 1,
                'permissions'=>'',
                'name' => 'superuser',
            ],
            [
                'id'   => 2,
                'permissions'=>'{"access.admin.backend":"1"}',
                'name' => 'admin',
            ],
            [
                'id'   => 3,
                'permissions'=>'',
                'name' => 'user',
            ],
            [
	            'id'   => 4,
                'permissions'=>'',
	            'name' => 'guest',
            ],
            [
	            'id'   => 5,
                'permissions'=>'{"access.public.frontend":"1"}',
	            'name' => 'registered',
            ],
        ]);

        DB::table('role_user')->insert([
            [
                'user_id' => 1,
                'role_id' => 1,
            ],
            [
                'user_id' => 1,
                'role_id' => 2,
            ],
            [
                'user_id' => 2,
                'role_id' => 2,
            ],
            [
                'user_id' => 3,
                'role_id' => 5,
            ],
        ]);
    }
}
