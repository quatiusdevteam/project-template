<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Litepie\User\Traits\Auth\RegisterAndLogin;
use Illuminate\Http\Request;
use Auth;
use Litepie\User\Traits\Auth\Common;

class LoginController extends Controller
{
    /*
     * |--------------------------------------------------------------------------
     * | Login Controller
     * |--------------------------------------------------------------------------
     * |
     * | This controller handles authenticating users for the application and
     * | redirecting them to your home screen. The controller uses a trait
     * | to conveniently provide its functionality to your applications.
     * |
     */
    use RegisterAndLogin;
    use AuthenticatesUsers;
    use Common;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
     * public function __construct()
     * {
     * $this->middleware('guest', ['except' => 'logout']);
     * }
     */
    protected $redirectTo = 'home';

    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    protected $guard = null;

    protected $username = 'email';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $guard = $request->get(config('user.params.type'), null);
        $this->setGuard($guard);
        // $this->setRedirectTo();
        $this->middleware('web');
        $this->middleware('guest:' . $guard, [
            'except' => [
                'logout',
                'verify',
                'sendVerification'
            ]
        ]);
        $this->setTheme();
        if ($guard == 'admin.web')
            $this->setupTheme(config('theme.map.admin.theme'), config('theme.map.admin.blank'));
        else
            $this->setupTheme(config('theme.map.public.theme'), config('theme.map.public.blank'));
    }

    function getCredentials(Request $request)
    {
        $login = array_merge($request->only($this->loginUsername(),'email', 'password'), [
                'status' => 'Active'
            ]);
        
        if ($this->loginUsername() != 'email' && $login['email'] && !$login[$this->loginUsername()]){
            $login[$this->loginUsername()] = $login['email'];
            unset($login['email']);
        }
            
        return $login;
    }

    /**
     * Show the user login form.
     *
     * @return \Illuminate\Http\Response
     */
    function showLoginForm()
    {
        $guard = $this->getGuard();
        $this->check($guard);
        return $this->theme->of($this->getView('login'), compact('guard'))
            ->render();
    }
    
    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }
    
    public function logout()
    {
        Auth::guard($this->getGuard())->logout();
        return redirect('/');
    }
    
    /**
     * Determine if the class is using the ThrottlesLogins trait.
     *
     * @return bool
     */
    function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(static::class)
            );
    }
    /*
     * Login base on access permission.
     *
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();
        
        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            
            return $this->sendLockoutResponse($request);
        }
        
        $allowLogin = false;
        $credentials = $this->getCredentials($request);
        
        if (Auth::guard($this->getGuard())->once($credentials)) {
            $user = Auth::guard($this->getGuard())->user();
            
            if ($this->getGuard() == "admin.web"){
                $allowLogin = $user->hasPermission('access.admin.backend') || $user->isSuperUser();
            } else {
				$allowLogin = $user->hasPermission(config('website.login.permission','access.public.frontend')) || $user->isSuperUser();
            }
        }
        
        if ($allowLogin) {
            $user->roles; // improve permission loading.
            $user->clients; // automatically load clients.
            
            Auth::guard($this->getGuard())->login($user, $request->has('remember'));
            return $this->handleUserWasAuthenticated($request, $throttles);
        }
        
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }
        
        return $this->sendFailedLoginResponse($request);
    }

    function authenticated($request, $user, $token)
    {
        return response()->json([
            'user' => $user,
            'request' => $request->all(),
            'token' => $token
        ]);
    }
}
