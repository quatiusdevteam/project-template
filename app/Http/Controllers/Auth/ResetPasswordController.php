<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Litepie\User\Traits\Auth\Common;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords, Common;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $guard = $request->get(config('user.params.type'), null);
        $this->setGuard($guard);
        $this->setRedirectTo();
        $this->setPasswordBroker();
        
        $this->middleware('guest');
        $this->setTheme();
        
        if ($guard == 'admin.web')
            $this->setupTheme(config('theme.map.admin.theme'), config('theme.map.admin.blank'));
            else
                $this->setupTheme(config('theme.map.public.theme'), config('theme.map.public.layout'));
    }
    
    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  string|null                 $token
     * @return \Illuminate\Http\Response
     */
    function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }
        
        return $this->theme->of($this->getView('reset'), compact('token'))
        ->render();
    }
}
