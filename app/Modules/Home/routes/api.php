<?php
Route::group(['prefix' => 'api', 'module' => 'Home', 'namespace' => 'App\Modules\Home\Controllers'], function() {

    Route::get('/', 'HomeApiController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
});