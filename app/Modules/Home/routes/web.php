<?php

Route::group(['module' => 'Home', 'middleware' => ['web'], 'namespace' => 'App\Modules\Home\Controllers'], function() {

    Route::get('/', 'HomeController@index')->name('home');
    
    Route::middleware('auth:web')->get('/home', 'HomeController@index')->name('home');
});
//Route::middleware('auth:admin.web')->get('/test', function(){return "OK";});
