<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
        	//View::addLocation(public_path(config('theme.themeDir').'/'.$theme->getThemeName().'/views'));
        	//View::addNamespace('admin', public_path(config('theme.themeDir').'/'.$theme->getThemeName().'/views'));
        	
        	//View::addLocation(public_path(config('theme.themeDir').'/'.$theme->getThemeName().'/views'));
        	//View::addNamespace('public', public_path(config('theme.themeDir').'/'.$theme->getThemeName().'/views'));
        	 
        	// You can remove this line anytime.
            $theme->setTitle(trans('cms.name'));

            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{!! $crumb["label"] !!}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{!! $crumb["label"] !!}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // You may use this event to set up your assets.
            // Partial composer.
            // $theme->partialComposer('header', function($view)
            // {
            //     $view->with('auth', Auth::user());
            // });

        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function($theme)
            {
            	$theme->asset()->add('summernote', 'css/summernote.css');
            	$theme->asset()->usepath()->add('bootstrap', 'css/theme.css');
            	$theme->asset()->usePath()->add('style', 'css/styles.css');
            	$theme->asset()->usepath()->add('skin', 'css/skins/skin-red.css');
            	$theme->asset()->add('icheck', 'packages/icheck/skins/square/blue.css');
            	 
            	$theme->asset()->add('jquery', 'packages/jquery/js/jquery.min.js');
            	$theme->asset()->container('footer')->add('scripts', 'js/admin.js');
            	$theme->asset()->container('footer')->usePath()->add('theme', 'js/theme.min.js');
            	$theme->asset()->container('footer')->add('icheck', 'packages/icheck/icheck.min.js');
            	// $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            },
            'blank' => function($theme)
            {

            	$theme->asset()->usePath()->add('style', 'css/styles.css');
            	$theme->asset()->usepath()->add('bootstrap', 'css/theme.css');
            	$theme->asset()->add('jquery', 'packages/jquery/js/jquery.min.js');

            	$theme->asset()->container('footer')->add('scripts', 'js/admin.js');
            },
            'ajax' => function($theme)
            {
            	
            }
        )

    )

);