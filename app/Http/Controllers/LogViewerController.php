<?php
namespace App\Http\Controllers;

use Rap2hpoutre\LaravelLogViewer\LogViewerController as LaravelLogViewerLogViewerController;
use Theme;

class LogViewerController extends LaravelLogViewerLogViewerController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->view_log = 'vendor.laravel-log-viewer.theme-log';
		return Theme::uses(config('theme.map.admin.theme'))
			->layout(config('theme.map.admin.layout'))
			->string(parent::index())->render();
	}
}
