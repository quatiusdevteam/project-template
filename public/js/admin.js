var baseUrl = "";
var incScripts = ["js/summernote-bookmark.js","js/summernote-video-attributes.js"];


function onScriptReady(){

}

$(function () {
    
    var basetmp = $('script[src$="/admin.js"]').attr('src');
    baseUrl = basetmp.substr(0, basetmp.length-11);

    incScripts.forEach((scriptUrl, index) => {
        $.getScript(baseUrl+scriptUrl, ()=>{
            if (index == incScripts.length - 1) onScriptReady();
        });
    });

    initMiniEditor($('.html-editor-mini'));

    initEditor($('.html-editor'));
    initEditor($('.html-editor[disabled]'), 'disable');
    jQuery.validator.setDefaults({
        debug: true,
        success: "valid",
        errorPlacement: function(error,element) {
            return true;
        }
    });
    
    $('input .pickadate').pickadate({
        format: 'dd mmm, yyyy',
        formatSubmit: 'yyyy-mm-dd',
        hiddenSuffix: '',
        selectMonths: true,
        selectYears: true
    }).prop('type','text');

    $('input .pickatime').pickatime({
        format: 'h:i A',
        formatSubmit: 'HH:i:00',
        hiddenSuffix: '',
        interval: 10,
        selectMonths: true,
        selectYears: true
    }).prop('type','text');

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-left",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    }).on('ifChanged', function (e) {
            $(this).trigger('change');
    });

    $('body').on('click', '[data-action]', function(e) {
        e.preventDefault();

        var $tag = $(this);

        if ($tag.data('action') == 'CREATE')
            return app.create($tag.data('form'), $tag.data('load-to'), $tag.data('datatable'));

        if ($tag.data('action') == 'UPDATE')
            return app.update($tag.data('form'), $tag.data('load-to'), $tag.data('datatable'));

        if ($tag.data('action') == 'DELETE'){
            return app.delete($tag.data('href'), $tag.data('load-to'), $tag.data('datatable'));
        }
        if ($tag.data('action') == 'REQUEST')
            return app.makeRequest($tag.data('method'), $tag.data('href'));

        app.load($tag.data('load-to'), $tag.data('href'));
    });

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
    });

    jQuery("time.timeago").timeago();
});


$( document ).ajaxComplete(function() {
    $("form[id$='-show'] :input").prop("disabled", true);

    initEditor($('.html-editor'));
    initEditor($('.html-editor[disabled]'), 'disable');

    initMiniEditor($('.html-editor-mini'));

    $('input .pickadate').pickadate({
        format: 'dd mmm, yyyy',
        formatSubmit: 'yyyy-mm-dd',
        hiddenSuffix: '',
        selectMonths: true,
        selectYears: true
    }).prop('type','text');

    $('input .pickatime').pickatime({
        format: 'h:i A',
        formatSubmit: 'HH:i:00',
        hiddenSuffix: '',
        interval: 10,
        selectMonths: true,
        selectYears: true
    }).prop('type','text');

    $.AdminLTE.boxWidget.activate()

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      }).on('ifChanged', function (e) {
              $(this).trigger('change');
    });
});


$( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
    app.message(jqxhr);
});

$( document ).ajaxSuccess(function( event, xhr, settings ) {
    app.message(xhr);
});


function cleanMsWord(txt, newLineRp) {
    /* https://github.com/DiemenDesign/summernote-cleaner */

    var out = txt;
    var keepClasses = false, keepHtml = true;
    var badTags = ['head', 'style', 'script', 'applet', 'embed', 'noframes', 'noscript']; //Remove full tags with contents
    var badAttributes = ['style', 'start', 'xmlns:o', 'xmlns:m', 'xmlns:w', 'xmlns'];
    var keepOnlyTags = [];

    if (!keepClasses) {
        var sS = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g;
        out = txt.replace(sS, ' ');
    }
    var nL = /(\n)+/g;
    out = out.replace(nL, newLineRp);
    if (keepHtml) {
        var cS = new RegExp('<!--(.*?)-->', 'gi');
        out = out.replace(cS, '');

        out = out.replace(/<!\[(.*?)\]>/g, '');

        var tS = new RegExp('<(/)*(meta|link|\\?xml:|st1:|o:|font)(.*?)>', 'gi');
        out = out.replace(tS, '');
        var bT = badTags;
        for (var i = 0; i < bT.length; i++) {
            tS = new RegExp('<' + bT[i] + '\\b.*>.*</' + bT[i] + '>', 'gi');
            out = out.replace(tS, '');
        }
        var allowedTags = keepOnlyTags;
        if (typeof (allowedTags) == "undefined") allowedTags = [];
        if (allowedTags.length > 0) {
            allowedTags = (((allowedTags || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
            var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
            out = out.replace(tags, function ($0, $1) {
                return allowedTags.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : ''
            });
        }
        var bA = badAttributes;
        for (var ii = 0; ii < bA.length; ii++) {
            var aS = new RegExp(' ' + bA[ii] + '=\'(.*?)\'', 'gi'); // single quote
            out = out.replace(aS, '');

            var aD = new RegExp(' ' + bA[ii] + '="(.*?)"', 'gi'); // double quotes
            out = out.replace(aD, '');
        }
    }
    return out;
};

function initEditor(itm, action){

   if (action){
       itm.summernote(action);
   }else{
       itm.summernote({
            height: 250,
            codemirror: {
                mode: "text/html",
                htmlMode: true,
                lineNumbers: true,
                textWrapping : true,
                theme: "eclipse",
                tabMode: 'indent'
            },
              
            fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '100', '120', '150'],
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname', 'fontsize', 'height']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', [, 'picture', 'videoAttributes','hr', 'link', 'bookmark']],
                ['view', ['fullscreen', 'codeview', 'help']],

            ],
            onImageUpload: function(files, editor, welEditable) {
                app.sendFile(files[0], editor, welEditable);
            },
            callbacks: {
                onPaste: function (evt) {
                    var ua = window.navigator.userAgent;
                    var msie = ua.indexOf("MSIE ");
                    msie = msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./);
                    var ffox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

                    if (msie)
                        var text = window.clipboardData.getData("Text");
                    else
                        var text = evt.originalEvent.clipboardData.getData(true ? 'text/html' : 'text/plain');

                    evt.preventDefault();

                    if (msie || ffox)
                        setTimeout(function () { document.execCommand('insertHtml', false, cleanMsWord(text, '<br>')); }, 1);
                    else
                        document.execCommand('insertHtml', false, cleanMsWord(text, '<br>'));
                }
            }
        });
       itm.on('init', function () {
       })

    }
}

function initMiniEditor(itm, action){

   if (action){
       itm.summernote(action);
   }else{
       itm.summernote({
            height: "100px",
            fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '100', '120', '150'],
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontname', 'fontsize']],
                ['color', ['color', 'link']],
                ['para', ['height', 'paragraph', 'codeview']],
                ['bookmark', ['hello']]
          ],
          callbacks: {
              onPaste: function (evt) {
                  var ua = window.navigator.userAgent;
                  var msie = ua.indexOf("MSIE ");
                  msie = msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./);
                  var ffox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

                  if (msie)
                      var text = window.clipboardData.getData("Text");
                  else
                      var text = evt.originalEvent.clipboardData.getData(true ? 'text/html' : 'text/plain');

                  evt.preventDefault();

                  if (msie || ffox)
                      setTimeout(function () { document.execCommand('insertHtml', false, cleanMsWord(text, '<br>')); }, 1);
                  else
                      document.execCommand('insertHtml', false, cleanMsWord(text, '<br>'));
              }
          }
        });
    }
}


var app = {

    'create' : function(forms, tag, datatable) {
        var form = $(forms);

        if(form.valid() == false) {
            toastr.error('Please enter valid information.', 'Error');
            return false;
        }

        var formData = new FormData($(forms)[0]);
        
        var url  = form.attr('action');

        $.ajax( {
            url: url,
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success:function(data, textStatus, jqXHR)
            {
                app.load(tag, data.redirect);
                $(datatable).DataTable().ajax.reload( null, false );
            }
        });
    },

    'update' : function(forms, tag, datatable) {
        var form = $(forms);

        if(form.valid() == false) {
            toastr.error('Please enter valid information.', 'Error');
            return false;
        }

        var formData = new FormData($(forms)[0]);
        
        var url  = form.attr('action');

        $.ajax( {
            url: url,
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success:function(data, textStatus, jqXHR)
            {
                app.load(tag, data.redirect);
                $(datatable).DataTable().ajax.reload( null, false );
            }
        });
    },

    'delete' : function(target, tag, datatable) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function(){
            var data = new FormData();
            $.ajax({
                url: target,
                type: 'DELETE',
                processData: false,
                contentType: false,
                dataType: 'json',
                success:function(data, textStatus, jqXHR)
                {
                    swal("Deleted!", data.message, "success");
                    app.load(tag, data.redirect);
                    $(datatable).DataTable().ajax.reload( null, false );
                },
                error:function(data, textStatus, jqXHR)
                {
                    swal("Delete failed!", data.message, "error");
                },
            });
        });
    },

    'load' : function(tag, target) {
        $(tag).load(target);
    },

    'sendFile' : function(file, url, editor) {
        var data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "POST",
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            success: function(objFile) {
                editor.summernote('insertImage', objFile.folder+objFile.file);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
            }
        });
    },

    'makeRequest' : function(method, target) {
        $.ajax({
            url: target,
            type: method,
            success:function(data, textStatus, jqXHR)
            {
                app.message(jqXHR);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                app.message(jqXHR);
            }
        });
    },

    'message' : function(info){
        var msg = {
            type:'info',
            title:'',
            text:'',
            code:info.status
        };
        var responseJson = {};
        try {
            responseJson = jQuery.parseJSON(info.responseText);
        } catch (error) {
            responseJson = {};
        }
        
        if (responseJson.code != undefined)
            msg.code = responseJson.code;

        if (responseJson.msg_code != undefined)
            msg.code = responseJson.msg_code;

        if (msg.code == 200) {
            return true;
        }

        if (msg.code == 201) {
            msg.title   = 'Success';
            msg.type    = 'success';
            if (responseJson.message != undefined)
                msg.text    = responseJson.message;
        }else if (msg.code == 422) {
            msg.type    = 'warning';
            msg.title   = info.statusText;
            $.each(responseJson, function(key, val){
                msg.text    += val + "<br>";
            });
        }else if (msg.code >= 100 && msg.code <= 199){
            msg.title   = 'Info';
            msg.type    = 'info';
            msg.text    = info.statusText;
        }else if (msg.code >= 202 && msg.code <= 299){
            msg.title   = 'Success';
            msg.type    = 'success';
            msg.text    = info.statusText;
        }else if (msg.code >= 400 && msg.code <= 499){
            msg.title   = 'Warning';
            msg.type    = 'warning';
            msg.text    = info.statusText;
        }else if (msg.code >= 500 && msg.code<= 599){
            msg.type    = 'error';
            msg.title   = 'Error';
            msg.text    = info.statusText;
        }

        if (responseJson.message != undefined)
            msg.text = responseJson.message;

        if (msg.type != null)
            toastr[msg.type](msg.text, msg.title);

        return true;
    }
}

