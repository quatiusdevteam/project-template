<!--Navigation-->
<div id="navigation">
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  <a class="navbar-brand" href="{{url('/')}}"><img style="height: 30px;display: inline-block;margin-right: 9px;" src="{!! Theme::asset()->url('img/logo.png'); !!}">Company Name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        
          <!-- include("Shop::search.form") -->
	      
	      {!!Menu::menu('main', 'public::menu.main')!!}	
	      
	      <form class="navbar-form navbar-left">
              	<div class="input-group">
                  <input id="search-item-input" name="search" autocomplete="off" type="text" class="form-control " value="" placeholder="Search for...">
                  <div class="search-item-result" style="display: none">
                  </div>
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">Go!</button>
                  </span>
                </div>
            </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
</div>
<!--Navigation-->