<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Str;
use Closure;
use Auth;
use JWTAuth;

class Authenticate extends Middleware
{
    private $guards = null;
     /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $this->guards = $guards;
        $this->guard = $this->guards?$this->guards[0]:null;

        if ($this->guard == 'api'){
            config()->set('auth.defaults.guard','api');

            $request->headers->set('Accept', 'application/json');
            $request->headers->set('HTTP_ACCEPT','application/json');
            if (Auth::guard($this->guard)->guest()){
                try {
                    $accessTokens = $this->basicToken($request);
                    if ($accessTokens){
                        $user = \App\User::whereEmail($accessTokens[0])->whereApiToken($accessTokens[1])->first();

                        if (!$user || !$this->doBasicLogin($user))
                            return response()->json(['status' => 'Token is Invalid'], 401);

                    }
                    else
                        JWTAuth::parseToken()->authenticate();
                    
                } catch (\Exception $e) {
                    if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                        return response()->json(['status' => 'Token is Invalid'], 401);
                    }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                        return response()->json(['status' => 'Token is Expired'], 401);
                    }else{
                        return response()->json(['status' => 'Authorization Token not found'], 401);
                    }
                }
            }
        }
        
        $this->authenticate($request, $guards);
        
        return $next($request);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {   
        if (! $request->expectsJson()) {
            return route("login",["role"=>isset($this->guards[0])?$this->guards[0]:""]);
        }
    }

    private function basicToken($request){
        $header = $request->header('Authorization', '');
        
        if (Str::startsWith($header, 'Basic ')) {
            $access = Str::substr($header, 6);
            $strDecode = base64_decode($access);
            $accessArray = explode(':', $strDecode);
            if (count($accessArray) == 2) return $accessArray;
        }
        
        return false;
    }

    private function doBasicLogin($user){
        if (!$user) return;

        Illuminate\Support\Facades\Auth::login($user);
        return $user;
    }
}
