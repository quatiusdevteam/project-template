@foreach ($menus as $menu)
	@if ($menu->active)
		<?php Theme::breadcrumb()->add($menu->name, $menu->url=='#'? '#': trans_url($menu->url)); ?>
		
		@if ($menu->hasChildren())
			@include('public::breadcrumb.tail', array('menus' => $menu->getChildren()))
		@endif
	@endif
@endforeach