<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([

            [
                'id'          => 1,
                'email'       => 'super@mail.com',
                'password'    => bcrypt('dev@2017'),
                'status'      => 'Active',
                'name'        => 'Super User',
                'sex'         => 'Male',
                'api_token'   => str_random(60),
                'designation' => 'Admin',
                'created_at'  => '2015-09-15',
            ],
            [
                'id'          => 2,
                'email'       => 'admin@mail.com',
                'password'    => bcrypt('dev@2017'),
                'status'      => 'Active',
                'name'        => 'Admin',
                'sex'         => 'Male',
                'api_token'   => str_random(60),
                'designation' => 'Admin',
                'created_at'  => '2015-09-15',
            ],
            [
                'id'          => 3,
                'email'       => 'test@mail.com',
                'password'    => bcrypt('dev@2017'),
                'status'      => 'Active',
                'name'        => 'User',
                'sex'         => 'Male',
                'api_token'   => str_random(60),
                'designation' => 'User',
                'created_at'  => '2015-09-15',
            ],
        ]);
    }
}
