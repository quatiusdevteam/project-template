<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('authenticate', 'Auth\LoginApiController@login');
Route::group(['middleware' => 'auth:api','prefix' => 'authenticate'], function ($router) {
    Route::get('refresh', 'Auth\LoginApiController@refresh');
    Route::get('user', 'Auth\LoginApiController@me');
    Route::get('logout', 'Auth\LoginApiController@logout');
});