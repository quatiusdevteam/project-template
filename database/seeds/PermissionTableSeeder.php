<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'slug'          => 'user.user.view',
                'name'          => 'View user',
            ],
            [
                'slug'          => 'user.user.create',
                'name'          => 'Create user',
            ],
            [
                'slug'          => 'user.user.edit',
                'name'          => 'Update user',
            ],
            [
                'slug'          => 'user.user.delete',
                'name'          => 'Delete user',
            ],
            [
                'slug'          => 'access.public.frontend',
                'name'          => 'Access Front-end',
            ],
            [
                'slug'          => 'access.admin.backend',
                'name'          => 'Access Back-end',
            ],
            [
                'slug'          => 'access.token.api',
                'name'          => 'Access via Token',
            ],
        ]);
    }
}
