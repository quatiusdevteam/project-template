<footer class="footer">
    <div class="container-fluid">
        <nav class="navbar navbar-default navbar-left">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">
                        Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
            </ul>
        </nav>
        <p class="copyright pull-right">
            Lavalite © 2017 All Rights Reserved. Powered by <a href="http://renfos.com" target="_blank"> Renfos Technologies Pvt Ltd.</a>
        </p>
    </div>
</footer>